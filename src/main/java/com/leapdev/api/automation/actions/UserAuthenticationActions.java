package com.leapdev.api.automation.actions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.json.JSONObject;
import io.restassured.RestAssured;

public class UserAuthenticationActions {
	
	public static String userAuthentication(String baseURL, String username, String passowrd, String Filename) throws IOException {
		String filePath = System.getProperty("user.dir") + "\\src\\main\\resources\\" + Filename;
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        JSONObject jsonObj = new JSONObject(content);
		jsonObj.put("userName", username);
		jsonObj.put("password", passowrd);
		String token = RestAssured.given().header("Content-Type", "application/json")
							.body(jsonObj.toString()).request()
							.post(baseURL + "/Account/v1/GenerateToken").path("token");
				return token.toString();
	}
}

package com.leapdev.api.automation.actions;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BookActions {
	
	public static Response getBooks(String getBookURL) {
		System.out.println("GET API call : Request to get books");
		Response response = RestAssured.given().header("Content-Type", "application/json")
				.request()
				.get(getBookURL);
		return response;
	}
	
	public static Response deleteAllBooks(String deleteURL, String token) {
		System.out.println("DELETE API call : Request to delete all books from user's collection");
		Response response = RestAssured.given().header("Content-Type", "application/json")
				.header("Authorization", "Bearer " + token)
				.request().delete(deleteURL);
		return response;
	}
	
	public static Response deleteABook(String baseURL, String token, String filename, String userId, String isbn) throws IOException {
		System.out.println("DELETE API call : Request to delete a book from user's collection");
		String filePath = System.getProperty("user.dir") + "\\src\\main\\resources\\" + filename;
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        JSONObject jsonObj = new JSONObject(content);
		jsonObj.put("userId", userId);
		jsonObj.put("isbn", isbn);
		
		Response response = RestAssured.given().header("Content-Type", "application/json")
				.header("Authorization", "Bearer " + token)
				.body(jsonObj.toString()).request().delete(baseURL + "/BookStore/v1/Book");
		return response;
	}
	
	public static Response updateBookList(String baseURL, String token, String filename, String oldIsbn, String userId, String updatedIsbn) throws IOException {
		System.out.println("PUT API call : Request to update a book from user's collection");
		String filePath = System.getProperty("user.dir") + "\\src\\main\\resources\\" + filename;
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        JSONObject jsonObj = new JSONObject(content);
		jsonObj.put("userId", userId);
		jsonObj.put("isbn", updatedIsbn);
		
		Response response = RestAssured.given().header("Content-Type", "application/json")
				.header("Authorization", "Bearer " + token)
				.body(jsonObj.toString()).request().put(baseURL + "/BookStore/v1/Books/" + oldIsbn);
		return response;
	}
	
	public static Response addABook(String baseURL, String token, String filename, String userId, String isbn) throws IOException {
		System.out.println("POST API call : Request to add a book to user's collection");
		String filePath = System.getProperty("user.dir") + "\\src\\main\\resources\\" + filename;
        String content = new String(Files.readAllBytes(Paths.get(filePath)));
        JSONObject jsonObj = new JSONObject(content);
        jsonObj.getJSONArray("collectionOfIsbns").getJSONObject(0).put("isbn", isbn);
		jsonObj.put("userId", userId);

		Response response = RestAssured.given().header("Content-Type", "application/json")
				.header("Authorization", "Bearer " + token)
				.body(jsonObj.toString()).request().post(baseURL + "/BookStore/v1/Books");
		return response;
	}
}
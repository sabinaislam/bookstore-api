package com.leapdev.api.automation.utility;

import org.testng.annotations.DataProvider;


public class TestDataProvider {
	
	@DataProvider(name="addABook")   
	public static Object[][] addABookIsbn(){
		return new Object[][] {
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449331818", 201 },
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449337711", 201 },
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781491904244", 201 },
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449331818Incorrect", 400 },
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "123", 400 },
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "", 400 }
		}; 
	}

	@DataProvider(name="updateBookList")   
	public static Object[][] updateBookIsbn(){
		return new Object[][] {
			{ "9781449331818", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449325862", 200 },
			{ "9781593277574", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449325862", 400 },
			{ "9781593277574Incorrect", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449325862", 400 },
			{ "1234", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449325862", 400 },
			{ "", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449325862", 404 },
			{ "9781449331818", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781593277574Incorrect", 400 },
			{ "9781449331818", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "1234", 400 },
			{ "9781449331818", "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "", 400 }
		}; 
	}	
	
	@DataProvider(name="deleteABook")   
	public static Object[][] deleteBookIsbn(){
		return new Object[][] {
			{"e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781449337711", 204 },
			{"e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781593277574", 400 },
			{"e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "9781593277574Incorrect", 400 },
			{"e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "1234", 400 },
			{"e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", "", 400 }
		}; 
	}
	
	@DataProvider(name="deleteAllBooks")   
	public static Object[][] userId(){
		return new Object[][] {
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", 204 }
		}; 
	}
	
	@DataProvider(name="getBooks")   
	public static Object[][] getBookStatusCode(){
		return new Object[][] {
			{ 200 }
		}; 
	}
	
	@DataProvider(name="getABook")   
	public static Object[][] getBookIsbn(){
		return new Object[][] {
			{ "9781449325862", 200, "isbn;title;author;publisher" },
			{ "9781449325862Incorrect", 400, "code;message" },
			{ "1234", 400, "code;message" },
			{ "", 400, "code;message" }

		}; 
	}

	@DataProvider(name="apiInSequence")   
	public static Object[][] apiSequnces(){
		return new Object[][] {
			{ "e8dd9c1a-0d99-4933-a61f-9d8a8eb3cd51", 201, 200, 204 }
		}; 
	}
}
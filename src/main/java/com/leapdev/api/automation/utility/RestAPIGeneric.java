package com.leapdev.api.automation.utility;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.util.List;
import org.testng.Assert;
import io.restassured.response.Response;


public class RestAPIGeneric {
	
	  public void verifyStatusCode(Response response, int expectedStatusCode){
		  int statusCodeResponseBody = response.getStatusCode();
		  assertEquals(statusCodeResponseBody, expectedStatusCode, "Status code mismatch, excpected: " + expectedStatusCode + "found: " +  statusCodeResponseBody); 
		  System.out.println("Status Code in Response:" + statusCodeResponseBody + " is equal to Expected Status Code: " + expectedStatusCode); 
	  }
	  
	  public void verifyResponseNotEmpty(Response response, String values) {
		  if (values.contains(";")) {
			  String[] value = values.split(";");
			  for (int i = 0; i < value.length; i++) {
				  assertTrue(!response.path(value[i]).toString().isEmpty());
			  }
		  } else {
			assertTrue(!response.path(values).toString().isEmpty());
		  }
	  }
	  
	  public void verifyBookInfo(Response response, String key, String expectedValue) {
		  List<String> items = response.getBody().jsonPath().getList(key);
		  Assert.assertTrue(items.contains(expectedValue), "Value not found");
	  }
	  
	  public void verifyBookInfo(Response response, String key) {
		  List<String> items = response.getBody().jsonPath().getList(key);
		  Assert.assertTrue(items.size() > 0, "Booklist is empty");
	  }
	  
	  public String getaBook(Response response, String key, int index) {
		  List<String> items = response.getBody().jsonPath().getList(key);
		  return items.get(index);
	  }
}
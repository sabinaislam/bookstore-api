package com.leapdev.api.automation.tests;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.response.Response;

import com.leapdev.api.automation.utility.TestDataProvider;
import com.leapdev.api.automation.utility.RestAPIGeneric;
import com.leapdev.api.automation.actions.BookActions;
import com.leapdev.api.automation.actions.UserAuthenticationActions;

public class BookTests {
	
	public static String baseURL;
	public Response response;
	public static String authToken;
	RestAPIGeneric restAPIGeneric = new RestAPIGeneric();
	
	@BeforeTest()	
	@Parameters({"baseurl", "username", "password", "loginFileName"})
	public void setup(String url, String username, String password, String fileName) throws InterruptedException, IOException{
		baseURL = url;
		authToken = UserAuthenticationActions.userAuthentication(url, username, password, fileName);
	}
	
	 @Test(priority = 1, description = "Add a Book", dataProvider = "addABook", dataProviderClass = TestDataProvider.class)
	 public void addABook(String userId, String isbn, int statusCodeTestData) throws IOException {
		 response = BookActions.addABook(baseURL, authToken, "addBook.json", userId, isbn);
		 restAPIGeneric.verifyStatusCode(response, statusCodeTestData);
		 if(statusCodeTestData == 201)
			 restAPIGeneric.verifyBookInfo(response, "books.isbn", isbn);
	 }
	 
	 @Test(priority = 2, description = "Update Book List", dataProvider = "updateBookList", dataProviderClass = TestDataProvider.class)
	 public void updateBookList(String oldIsbn, String userId, String updatedIsbn, int statusCodeTestData) throws IOException {
		 response = BookActions.updateBookList(baseURL, authToken, "updateBookList.json", oldIsbn, userId, updatedIsbn);
		 restAPIGeneric.verifyStatusCode(response, statusCodeTestData);
		 if(statusCodeTestData == 200)
			 restAPIGeneric.verifyBookInfo(response, "books.isbn", updatedIsbn);
	 }
	 
	 @Test(priority = 3, description = "Delete a Book", dataProvider = "deleteABook", dataProviderClass = TestDataProvider.class)
	 public void deleteABook(String userId, String isbn, int statusCodeTestData) throws IOException {
		 response = BookActions.deleteABook(baseURL, authToken, "deleteABook.json", userId, isbn);
		 restAPIGeneric.verifyStatusCode(response, statusCodeTestData);
	 }
	 
	 @Test(priority = 4, description = "Delete all Books", dataProvider = "deleteAllBooks", dataProviderClass = TestDataProvider.class)
	 public void deleteAllBooks(String userId, int statusCodeTestData) {
		 response = BookActions.deleteAllBooks(baseURL + "/BookStore/v1/Books?UserId=" + userId, authToken);
		 restAPIGeneric.verifyStatusCode(response, statusCodeTestData);
	 }
	 
	 @Test(priority = 5, description = "Get All Books", dataProvider = "getBooks", dataProviderClass = TestDataProvider.class)
	 public void getBooks(int statusCodeTestData) {
		 response = BookActions.getBooks(baseURL + "/BookStore/v1/Books");
		 restAPIGeneric.verifyStatusCode(response, statusCodeTestData);
		 restAPIGeneric.verifyBookInfo(response, "books.isbn");
	 }
	
	 @Test(priority = 6, description = "Get A Book", dataProvider = "getABook", dataProviderClass = TestDataProvider.class)
	 public void getABook(String isbn, int statusCodeTestData, String keys) throws InterruptedException{
		 response = BookActions.getBooks(baseURL + "/BookStore/v1/Book?ISBN=" + isbn);
		 restAPIGeneric.verifyStatusCode(response, statusCodeTestData);
		 restAPIGeneric.verifyResponseNotEmpty(response, keys);
	 }
	 
	 @Test(priority = 7, description = "Run all BookAPIs", dataProvider = "apiInSequence", dataProviderClass = TestDataProvider.class)
	 public void apiInSequence(String userId, int created, int get, int delete) throws IOException {
		 response = BookActions.getBooks(baseURL + "/BookStore/v1/Books");
		 restAPIGeneric.verifyStatusCode(response, get);
		 restAPIGeneric.verifyBookInfo(response, "books.isbn");
		 String originalIsbn = restAPIGeneric.getaBook(response, "books.isbn", 0);
		 String isbnToUpdate = restAPIGeneric.getaBook(response, "books.isbn", 1);
		 
		 response = BookActions.addABook(baseURL, authToken, "addBook.json", userId, originalIsbn);
		 restAPIGeneric.verifyStatusCode(response, created);
		 restAPIGeneric.verifyBookInfo(response, "books.isbn", originalIsbn);
		 originalIsbn = restAPIGeneric.getaBook(response, "books.isbn", 0);
		 
		 response = BookActions.updateBookList(baseURL, authToken, "updateBookList.json", originalIsbn, userId, isbnToUpdate);
		 restAPIGeneric.verifyStatusCode(response, get);
		 restAPIGeneric.verifyBookInfo(response, "books.isbn", isbnToUpdate);
		 isbnToUpdate = restAPIGeneric.getaBook(response, "books.isbn", 0);

		 response = BookActions.deleteABook(baseURL, authToken, "deleteABook.json", userId, isbnToUpdate);
		 restAPIGeneric.verifyStatusCode(response, delete);
	 }

	@AfterTest()
	public void cleanup() throws InterruptedException{
		authToken = "";
	}
	 
	@AfterClass()
	public void teardown() throws InterruptedException{
	
	}	
}
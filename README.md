# Automate Bookstore API

## Prerequisite

- Java (test scripts ran using version: 1.8.0_241)
- Maven
- Git


## Environment Setup

- Clone this repository 


## Test Execution

- Run 'mvn clean test' from command prompt or in preferable IDE. 
- Alternatively, you can also run as a TestNG suite from your preferred IDE. 


## Report

- Open index.html file from 'target\surefire-reports\html' in a browser 
